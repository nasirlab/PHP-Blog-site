<?php
	include "../lib/session.php";
	Session::checklogin();
	include "../config/config.php";
	include "../lib/Database.php";
	include "../helpers/Format.php";
	$db = new Database();
	$fm = new Format();
?>


<!DOCTYPE html>
<head>
<meta charset="utf-8">
<title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/stylelogin.css" media="screen" />
</head>
<body>
<div class="container">
<section id="content">
<?php
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$username = $fm->validation($_POST['username']);
		$password = $fm->validation(md5($_POST['pass']));

		$userna = mysqli_real_escape_string($db->link,$username);
		$passwo = mysqli_real_escape_string($db->link,$password);

		$query = "SELECT * FROM tbl_user WHERE username ='$userna' AND pass = '$passwo' ";
		$result = $db->select($query);

		if ($result) {
			$data = $result->fetch_assoc();
			$row  = mysqli_num_rows($data);
			Session::set("login",true);
			Session::set("username",$data['username']);
			Session::set("userid",$data['id']);
			Session::set("userRole",$data['role']);
			header("Location: index.php");
		} else { 
			echo "<p style='color:red;'>Username or Password Not Matched !</p>";
   }
	}
?>
		<form action="" method="POST">
			<h1>Admin Login</h1>
			<div>
				<input type="text" placeholder="Username" required="" name="username"/>
			</div>
			<div>
				<input type="password" placeholder="Password" required="" name="pass"/>
			</div>
			<div>
				<input type="submit" value="Log in" />
			</div>
		</form><!-- form -->
		<div class="button">
			<a href="#">Welcome...</a>
		</div><!-- button -->
	</section><!-- content -->
</div><!-- container -->
</body>
</html>