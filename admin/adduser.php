<?php
    include "inc/header.php";
?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Add New Post User</h2>
                <div class="block">  
<?php 


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $username = $fm->validation($_POST['username']);
    $password = $fm->validation(md5($_POST['pass']));

    $username_ok = mysqli_real_escape_string ($db->link, $username);
    $password_ok = mysqli_real_escape_string($db->link, $password);
    $role        = mysqli_real_escape_string($db->link, $_POST['role']);

    if($username_ok== " " ||  $password_ok=="" || $role=="" ) {
        $add_msg =  "Filed can't be empty";
    }else{
        $query = "INSERT INTO tbl_user (username,pass,role) VALUES('$username_ok','$password_ok','$role')";
        $Add_user = $db->insert($query);
        if ($Add_user) {
            echo "User succesfully added";
        }else{
            echo "User not added";
        }
    }
}

?>

                 <form action="" method="POST">
                    <table class="form">                       
                        <tr>
                            <td>
                                <label>Username:</label>
                            </td>
                            <td>
                                <input type="text" name="username" placeholder="Enter user name..." class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Password:</label>
                            </td>
                            <td>
                                <input type="Password" name="pass" placeholder="Enter Password..." class="medium" />
                            </td>
                    </tr>
                            <tr>
                        <?php if (isset($c_error)) {
                           echo $c_error;
                        } ?>
                        <td>
                            <label>User role:</label>
                        </td>
                        <td>
                           <select id="select" name="role"> 
                                <option value="">Select any one</option>  
                                <option value="0">Admin</option>  
                                <option value="1">Author</option>  
                                <option value="2">Editor</option>  
                            </select>
                        </td>
                    </tr>
						<tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php
        include "inc/footer.php";
    ?>
