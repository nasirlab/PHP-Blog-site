<?php
    include "inc/header.php";
?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Add New Post</h2>
                <div class="block">  
<?php 


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $title      = mysqli_real_escape_string ($db->link, $_POST['title']);
    $body       = mysqli_real_escape_string($db->link, $_POST['body']);

    if($title== " " ||  $body=="" ) {
        echo "Filed can't be empty";
    }else{
        $query = "INSERT INTO tbl_page (pageName,body) VALUES('$title','$body')";
        $insert_data = $db->insert($query);
        if ($insert_data) {
            echo "Page added succesfully complet";
        }else{
            echo "Page not added";
        }
    }
}

?>

                 <form action="" method="POST" enctype="multipart/form-data">
                    <table class="form">                       
                        <tr>
                            <td>
                                <label>Title</label>
                            </td>
                            <td>
                                <input type="text" name="title" placeholder="Enter Post Title..." class="medium" />
                            </td>
                        </tr>

                        <tr>
                            <td style="vertical-align: top; padding-top: 9px;">
                                <label>Content</label>
                            </td>
                            <td>
                                <textarea class="tinymce" name="body"></textarea>
                            </td>
                        </tr>
						<tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php
        include "inc/footer.php";
    ?>
