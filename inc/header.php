<?php 
	include "config/config.php";
	include "helpers/format.php";
	include "lib/Database.php";
	$db = new Database();
	$fm = new Format();

	//Show logo title and slogan
	$query = "SELECT * FROM title_logo WHERE id = '1' ";
	$showData = $db->select($query);
	if ($showData) {
		$result = $showData->fetch_assoc();
	}else{
		$msg = "<p style='color:red'> Title Slogan and Logo Not Found</p>";
	}
	//link with social media
	$data 	= "SELECT * FROM tbl_social WHERE id = '1' ";
	$socialdata = $db->select($data);
	if ($socialdata) {
		$sociallink = $socialdata->fetch_assoc();
	}

?> 

<!DOCTYPE html>
<html>
<head>
	<?php if(isset($_GET['pageView'])){	//show page title in title tag
		$pageID = $_GET['pageView'];
		$titlequery = "SELECT * FROM tbl_page WHERE id = '$pageID' ";
		$showTitle  = $db->select($titlequery);
		if ($showTitle) {
		while ($resultTitle = $showTitle->fetch_assoc()) { ?>
		<title><?php echo $resultTitle['pageName'];?>-<?php echo TITLE;?></title>

		<?php } } }elseif(isset($_GET['postID'])){	//show post title in title tag
			$postID = $_GET['postID'];
			$postquery = "SELECT * FROM tbl_post WHERE id = '$postID' ";
			$postTitle  = $db->select($postquery);
			if ($postTitle) {
				while ($resultpost = $postTitle->fetch_assoc()) { ?>
				<title><?php echo $resultpost['title'];?>-<?php echo TITLE;?></title>
		<?php } } }else{ ?>	
			<title><?php echo $fm->title();?>-<?php echo TITLE;?></title>
			<?php	} //show Fixed  page title in title tag ?>
	<meta name="language" content="English">
	<meta name="description" content="It is a website about education">
	<meta name="keywords" content="blog,cms blog">
	<meta name="author" content="Delowar">
	<link rel="stylesheet" href="font-awesome-4.5.0/css/font-awesome.css">	
	<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="style.css">
	<script src="js/jquery.js" type="text/javascript"></script>
	<script src="js/jquery.nivo.slider.js" type="text/javascript"></script>

<script type="text/javascript">
$(window).load(function() {
	$('#slider').nivoSlider({
		effect:'random',
		slices:10,
		animSpeed:500,
		pauseTime:5000,
		startSlide:0, //Set starting Slide (0 index)
		directionNav:false,
		directionNavHide:false, //Only show on hover
		controlNav:false, //1,2,3...
		controlNavThumbs:false, //Use thumbnails for Control Nav
		pauseOnHover:true, //Stop animation while hovering
		manualAdvance:false, //Force manual transitions
		captionOpacity:0.8, //Universal caption opacity
		beforeChange: function(){},
		afterChange: function(){},
		slideshowEnd: function(){} //Triggers after all slides have been shown
	});
});
</script>
</head>

<body>
	<div class="headersection templete clear">

		<?php if (isset($msg)) { echo $msg;	} ?>

		<a href="index.php">
			<div class="logo">
				<img src="admin/<?php echo $result['logo'];?>" alt="Logo"/>
				<h2><?php echo $result['title']; ?></h2>
				<p><?php echo $result['slogan']; ?></p>
			</div>
		</a>
		<div class="social clear">
			<div class="icon clear">
				<a href="<?php echo $sociallink['facebook'];?>" target="_blank"><i class="fa fa-facebook"></i></a>
				<a href="<?php echo $sociallink['twitter'];?>" target="_blank"><i class="fa fa-twitter"></i></a>
				<a href="<?php echo $sociallink['linkedin'];?>" target="_blank"><i class="fa fa-linkedin"></i></a>
				<a href="<?php echo $sociallink['googleplus'];?>" target="_blank"><i class="fa fa-google-plus"></i></a>
			</div>
			<div class="searchbtn clear">
			<form action="search.php" method="POST">
				<input type="text" name="search" placeholder="Search keyword..."/>
				<input type="submit" name="submit" value="Search"/>
			</form>
			</div>
		</div>
	</div>

<?php include "menu.php";?>